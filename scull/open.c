#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	if (argc != 2) {
		(void)fprintf(stderr, "Invalid command line arguments\n");
		return EXIT_FAILURE;
	}

	if (0 >= open(argv[1], 0, O_RDONLY)) {
		perror("open");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
