#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/kdev_t.h>
#include <linux/module.h>
#include <linux/types.h>


#define SUCCESS 0

#define SCULL_MINOR 0

#define SCULL_COUNT 1


static int scull_open(struct inode *inode, struct file *file);

static int scull_release(struct inode *inode, struct file *file);


static dev_t scull_dev = MKDEV(0, 0);

static bool scull_inited = false;

static struct file_operations scull_fops = {
	.owner   = THIS_MODULE,
	.open    = scull_open,
	.release = scull_release
};

static struct cdev scull_cdev = {
	.owner = THIS_MODULE,
	.ops   = &scull_fops
};


static int __init scull_init(void)
{
	if (alloc_chrdev_region(&scull_dev, SCULL_MINOR, SCULL_COUNT, "scull_dev")) {
		(void)printk(KERN_DEBUG "scull_dev: alloc_chrdev_region error\n");
		goto cleanup_alloc;
	}

	cdev_init(&scull_cdev, &scull_fops);
	if (cdev_add(&scull_cdev, scull_dev, SCULL_COUNT)) {
		(void)printk(KERN_DEBUG "scull_dev: alloc_add error\n");
		goto cleanup_add;
	}

	scull_inited = true;
	return 0;

cleanup_add:
	unregister_chrdev_region(scull_dev, SCULL_COUNT);
cleanup_alloc:
	scull_inited = false;
	return 0;
}

static void __exit scull_exit(void)
{
	if (scull_inited) {
		cdev_del(&scull_cdev);
		unregister_chrdev_region(scull_dev, SCULL_COUNT);
	}
}

static int scull_open(struct inode *inode, struct file *file)
{
	(void)printk(KERN_DEBUG "scull_dev: opened inode = %p, file = %p\n",
			(void *)inode, (void *)file);
	return SUCCESS;
}

static int scull_release(struct inode *inode, struct file *file)
{
	(void)printk(KERN_DEBUG "scull_dev: released inode = %p, file = %p\n",
			(void *)inode, (void *)file);
	return SUCCESS;
}


MODULE_LICENSE("Dual BSD/GPL");
module_init(scull_init);
module_exit(scull_exit);
